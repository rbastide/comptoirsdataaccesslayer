package comptoirs.model;

import comptoirs.model.dao.MyDAO;
import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import comptoirs.model.entity.Categorie;
import comptoirs.model.entity.Produit;
import static org.junit.Assert.assertTrue;

public class DaoTest {
	EntityManagerFactory emFactory;
	MyDAO dao;

	@Before
	public void setUp() {
		emFactory = Persistence.createEntityManagerFactory("test");
		dao = new MyDAO(emFactory);
	}
	
	@After
	public void tearDown() {
		emFactory.close();
	}

	@Test
	public void testCreateCategorie() {
		Categorie cat = dao.createCategorie("Essai description", "essai Libellé");
		// D'après le schéma, les clés auto-générées commencent à 9
		assertEquals("La clé doit avoir été auto-générée", 9, (int)cat.getCode());
	}	
	
}
