package comptoirs.model;

import comptoirs.model.generateddao.CategorieJpaController;
import comptoirs.model.entity.Categorie;
import comptoirs.model.generateddao.exceptions.IllegalOrphanException;

import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

public class CategorieJpaControllerTest {
	EntityManagerFactory emFactory;
	CategorieJpaController dao;	
	
	
	@Before
	public void setUp() {
		emFactory = Persistence.createEntityManagerFactory("test");
		dao = new CategorieJpaController(emFactory);
	}
	
	@After
	public void tearDown() {
		dao = null;
		emFactory.close();
	}

	/**
	 * Test of create method, of class CategorieJpaController.
	 */
	@Test
	public void testCreate() {
		System.out.println("create");
		Categorie cat = new Categorie();
		cat.setDescription("Test categorie");
		cat.setLibelle("ceci est un test");
		dao.create(cat);
		// D'après le schéma, les clés auto-générées commencent à 9
		assertEquals("Le code n'a pas été correctement auto-généré", 9, cat.getCode().intValue());
	}

	/**
	 * Test of create method, of class CategorieJpaController.
	 */
	@Test
	public void testImpossibleCreerDoublon() {
		System.out.println("create doublon");
		Categorie cat = new Categorie();
		cat.setDescription("Test categorie");
		cat.setLibelle("Boissons"); // libellé UNIQUE, existe déjà
		try {
			dao.create(cat);
			fail("La catégorie 'Boissons' existe déjà dans le jeu de test, création impossible");
		} catch(Exception e) {
			assertTrue("On doit recevoir une violation de contrainte d'intégrité",
				e.getMessage().contains("integrity constraint violation"));
		}
	}
	
	/**
	 * Test of findCategorie method, of class CategorieJpaController.
	 */
	@Test 
	public void testFindCategorie() {
		System.out.println("findCategorie");
		Integer id = 1 ;
		Categorie result = dao.findCategorie(id);
		assertEquals("La catégorie n'a pas été correctement retrouvée", "Boissons", result.getLibelle());
		//assertEquals("La catégorie devrait avoir deux produits", 2, result.getProduitCollection().size());
	}
	
	/**
	 * Test of edit method, of class CategorieJpaController.
	 */
	@Test
	public void testEdit() throws Exception {
		System.out.println("edit");
		Integer id = 1;
		Categorie original = dao.findCategorie(id);		
		original.setLibelle("Essai");
		dao.edit(original);
		Categorie updated = dao.findCategorie(id);
		assertEquals(original, updated);
	}

	/**
	 * Test of destroy method, of class CategorieJpaController.
	 */
	@Test(expected = IllegalOrphanException.class)
	public void testDestroyCategorieQuiADesProduits() throws Exception {
		System.out.println("destroy");
		Integer codeBoissons = 1;
		// On ne peut pas détruire cette catégorie, car elle a des produits,
		// On doit avoir une exception
		dao.destroy(codeBoissons);
	}

	/**
	 * Test of destroy method, of class CategorieJpaController.
	 */
	@Test
	public void testDestroyCategorieSansProduits() throws Exception {
		System.out.println("destroy");
		Integer codeCondiments = 2;
		dao.destroy(codeCondiments);
		assertEquals("Il ne doit rested qu'une catégorie", 1, dao.findCategorieEntities().size() );
	}

	/**
	 * Test of findCategorieEntities method, of class CategorieJpaController.
	 */
	@Test
	public void testFindCategorieEntities() {
		System.out.println("findCategorieEntities");
		Categorie boissons = dao.findCategorie(1);
		Categorie condiments = dao.findCategorie(2);
		List<Categorie> result = dao.findCategorieEntities();
		assertEquals("Il y a deux catégories dans le jeu de test", 2, result.size());
		assertTrue("La catégorie 'Boissons' existe dans le jeu de test", result.contains(boissons));
		assertTrue("La catégorie 'Condiments' existe dans le jeu de test", result.contains(condiments));
	}

	/**
	 * Test of getCategorieCount method, of class CategorieJpaController.
	 */
	@Test
	public void testGetCategorieCount() {
		System.out.println("getCategorieCount");
		int expResult = 2;
		int result = dao.getCategorieCount();
		assertEquals("Il y a deux catégories dans le jeu de test", expResult, result);
	}
	
}
