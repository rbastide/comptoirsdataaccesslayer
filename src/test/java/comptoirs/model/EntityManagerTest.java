package comptoirs.model;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import comptoirs.model.entity.Categorie;
import comptoirs.model.entity.Produit;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author rbastide
 */
public class EntityManagerTest {
	EntityManagerFactory emFactory;
	EntityManager em;	

	@Before
	public void setUp() {
		emFactory = Persistence.createEntityManagerFactory("test");
		em = emFactory.createEntityManager();
	}
	
	@After
	public void tearDown() {
		em.close();
		emFactory.close();
	}

	@Test
	public void testCreationCategorieAvecProduits() {
		Categorie cat = new Categorie();
		cat.setDescription("logiciel");
		cat.setLibelle("Logiciels et utilitaires");
		Produit p1 = new Produit();
		p1.setNom("Windows 10"); p1.setPrixUnitaire(BigDecimal.ZERO);
		p1.setCategorie(cat);
		Produit p2 = new Produit();
		p2.setNom("Norton Antivirus"); p2.setPrixUnitaire(BigDecimal.TEN);
		p2.setCategorie(cat);
		Collection<Produit> produits = new ArrayList<>();
		produits.add(p1);
		produits.add(p2);
		cat.setProduitCollection(produits);
		System.out.println("On va enregistrer la catégorie et ses 2 produits");
		em.getTransaction().begin();
		em.persist(cat);
		em.getTransaction().commit();
	}

	@Test
	public void testRechercheCategorieAvecProduits() {
		// Recherche par clé
		Categorie cat = em.find(Categorie.class, 1);
		// On a chargé également tous les produits de cette catégorie
		assertEquals("La catégorie 1 a 2 produits dans le jeu de test",
			2, cat.getProduitCollection().size());
		
		for (Produit p : cat.getProduitCollection())
			System.out.printf("Produit n° %d, nom %s %n",
				p.getReference(), p.getNom());
	}

}
