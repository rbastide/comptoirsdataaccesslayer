package comptoirs.model;

import comptoirs.model.generateddao.ClientJpaController;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

public class ClientJpaControllerTest {

	EntityManagerFactory emFactory;
	ClientJpaController dao;

	public ClientJpaControllerTest() {
	}

	@Before
	public void setUp() {
		emFactory = Persistence.createEntityManagerFactory("test");
		dao = new ClientJpaController(emFactory);
	}

	@After
	public void tearDown() {
		dao = null;
		emFactory.close();
	}

	/**
	 * Test of destroy method, of class ClientJpaController.
	 */
	@Test
	public void testDestroyClientSansCommande() throws Exception {
		System.out.println("destroy");
		String idClientSansCommande = "ALFKI";
		dao.destroy(idClientSansCommande);
		assertEquals("Il ne doit rester qu'un client", 1, dao.getClientCount());		
	}

	/**
	 * Test of destroy method, of class ClientJpaController.
	 */
	@Test @Ignore
	public void testDestroyClientAvecCommande() throws Exception {
		System.out.println("destroy");
		String idClientAvecCommande = "BONAP";
		dao.destroy(idClientAvecCommande);
		assertEquals("Il ne doit rester qu'un client", 1, dao.getClientCount());		
	}

}
