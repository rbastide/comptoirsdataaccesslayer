package comptoirs.model;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import comptoirs.model.entity.Categorie;
import comptoirs.model.entity.Client;
import comptoirs.model.entity.Commande;
import comptoirs.model.entity.Produit;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author rbastide
 */
public class QueriesTest {
	EntityManagerFactory emFactory;
	EntityManager em;	

	@Before
	public void setUp() {
		emFactory = Persistence.createEntityManagerFactory("test");
		em = emFactory.createEntityManager();
	}
	
	@After
	public void tearDown() {
		em.close();
		emFactory.close();
	}

	@Test
	public void testFindProduitQuery() {
		final List<Produit> produits = em.createNamedQuery("Produit.findAll", Produit.class)
			.getResultList();
		assertEquals("Il y a deux produits dans le jeu de test", 2, produits.size());	
		Produit p1 = produits.get(0);
		Categorie c1 = p1.getCategorie();
		assertEquals("Tous les produits du jeu de test sont dans la catégories 'Boissons'",
			"Boissons", c1.getLibelle());	
	}

	@Test
	public void testDeleteClientSansCommande() {
		final Client alfki = em.createNamedQuery("Client.findByCode", Client.class)
			.setParameter("code", "ALFKI")
			.getSingleResult();
		
		em.getTransaction().begin();
		em.remove(alfki);
		em.getTransaction().commit();
		
		final List<Client> clients = em.createNamedQuery("Client.findAll", Client.class).getResultList();		
		assertEquals("Il ne doit rester qu'un client", 1, clients.size());	
	}
	
	@Test
	public void testDeleteClientAvecCommande() {
		final Client bonap = em.createNamedQuery("Client.findByCode", Client.class)
			.setParameter("code", "BONAP")
			.getSingleResult();
		em.getTransaction().begin();
		em.remove(bonap);
		em.getTransaction().commit();
		
		final List<Client> clients = em.createNamedQuery("Client.findAll", Client.class).getResultList();		
		assertEquals("Il ne doit rester qu'un client", 1, clients.size());	
		
		final List<Commande> commandes = em.createNamedQuery("Commande.findAll", Commande.class).getResultList();		
		assertTrue("Toutes les commandes du jeu de test doivent être détruites", commandes.isEmpty());	

	}	
}
