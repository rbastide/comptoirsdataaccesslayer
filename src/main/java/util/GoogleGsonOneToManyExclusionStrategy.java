package util;

import javax.persistence.OneToMany;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * Pour customiser la génération de JSON
 * @see https://www.baeldung.com/gson-exclude-fields-serialization
 */
public class GoogleGsonOneToManyExclusionStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getAnnotation(OneToMany.class) != null;	
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}
	
}
