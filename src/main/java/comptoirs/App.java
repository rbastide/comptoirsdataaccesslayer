package comptoirs;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import comptoirs.model.entity.Categorie;
import comptoirs.model.entity.Produit;
import util.GoogleGsonOneToManyExclusionStrategy;

public class App {

	private App() {
	}

	/**
	 * Hello JPA !
	 *
	 * @param args The arguments of the program.
	 */
	public static void main(final String[] args) {
		// Générer du JSON
		// Gson gson = new Gson();
		// setPrettyPrinting pour que le JSON généré soit plus lisible
		Gson gson = new GsonBuilder()
			.setPrettyPrinting()
			.addSerializationExclusionStrategy(new GoogleGsonOneToManyExclusionStrategy())
			.create();
		
		final EntityManagerFactory emFactory = Persistence.createEntityManagerFactory("comptoirs");
		EntityManager em = emFactory.createEntityManager();
		System.out.println("On cherche tous les produits");
		Collection<Produit> produits = em.createNamedQuery("Produit.findAll", Produit.class)
			.getResultList();
		//final CategorieJpaController categoryDAO = new CategorieJpaController(emFactory);
		// categoryDAO.getEntityManager().getTransaction().begin();
		// List<Categorie> categories = categoryDAO.findCategorieEntities();
		System.out.printf("Il y a %d produits dans la base %n", produits.size());
		//Categorie cat = categoryDAO.findCategorie(1);
		Categorie cat = em.createNamedQuery("Categorie.findByCode", Categorie.class)
			        .setParameter("code", 1).getSingleResult();
		Collection<Produit> prods = cat.getProduitCollection();
		System.out.printf("Il y a %d produits dans la catégorie '%s' %n", prods.size(), cat.getDescription());
		//Produit p = produits.iterator().next();

		System.out.println(
			gson.toJson(cat)
		);

		//categoryDAO.getEntityManager().getTransaction().commit();
		em.close();
		emFactory.close();
	}

}
