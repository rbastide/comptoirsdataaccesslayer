package comptoirs.model.dao;

import comptoirs.model.entity.Categorie;
import comptoirs.model.entity.Produit;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class MyDAO {
	private EntityManagerFactory emf = null;

	public MyDAO(EntityManagerFactory emf) {
		this.emf = emf;
	}
	
	public Categorie createCategorie(String desc, String lib) {
		EntityManager em = emf.createEntityManager();

		Categorie result = new Categorie();
		result.setDescription(desc);
		result.setLibelle(lib);
		em.getTransaction().begin();
		em.persist(result);
		em.getTransaction().commit();
		em.close();
		return result;
	}	
	
	
	/**
	 * Insère dans la base un nouveau produit, en l'affectant à la catégorie
	 * @param nom le nom du produit
	 * @param codeCategorie le code d'une catégorie existante pour le nouveau produit
	 * @return le produuit nouvellement créé
	 * @throws java.lang.Exception si la catégorie n'existe pas
	 **/
	// TODO : implémenter et tester cette méthode (cas normal et cas d'erreur)
	public Produit createProduit(String nom, int codeCategorie) throws Exception {
		throw new UnsupportedOperationException("Pas encore implémenté");
	}
}
