/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comptoirs.model.generateddao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import comptoirs.model.entity.Client;
import comptoirs.model.entity.Commande;
import comptoirs.model.entity.Ligne;
import comptoirs.model.generateddao.exceptions.IllegalOrphanException;
import comptoirs.model.generateddao.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author RemiB
 */
public class CommandeJpaController implements Serializable {

	public CommandeJpaController(EntityManagerFactory emf) {
		this.emf = emf;
	}
	private EntityManagerFactory emf = null;

	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public void create(Commande commande) {
		if (commande.getLigneCollection() == null) {
			commande.setLigneCollection(new ArrayList<Ligne>());
		}
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Client client = commande.getClient();
			if (client != null) {
				client = em.getReference(client.getClass(), client.getCode());
				commande.setClient(client);
			}
			Collection<Ligne> attachedLigneCollection = new ArrayList<Ligne>();
			for (Ligne ligneCollectionLigneToAttach : commande.getLigneCollection()) {
				ligneCollectionLigneToAttach = em.getReference(ligneCollectionLigneToAttach.getClass(), ligneCollectionLigneToAttach.getLignePK());
				attachedLigneCollection.add(ligneCollectionLigneToAttach);
			}
			commande.setLigneCollection(attachedLigneCollection);
			em.persist(commande);
			if (client != null) {
				client.getCommandeCollection().add(commande);
				client = em.merge(client);
			}
			for (Ligne ligneCollectionLigne : commande.getLigneCollection()) {
				Commande oldCommande1OfLigneCollectionLigne = ligneCollectionLigne.getCommande1();
				ligneCollectionLigne.setCommande1(commande);
				ligneCollectionLigne = em.merge(ligneCollectionLigne);
				if (oldCommande1OfLigneCollectionLigne != null) {
					oldCommande1OfLigneCollectionLigne.getLigneCollection().remove(ligneCollectionLigne);
					oldCommande1OfLigneCollectionLigne = em.merge(oldCommande1OfLigneCollectionLigne);
				}
			}
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public void edit(Commande commande) throws IllegalOrphanException, NonexistentEntityException, Exception {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Commande persistentCommande = em.find(Commande.class, commande.getNumero());
			Client clientOld = persistentCommande.getClient();
			Client clientNew = commande.getClient();
			Collection<Ligne> ligneCollectionOld = persistentCommande.getLigneCollection();
			Collection<Ligne> ligneCollectionNew = commande.getLigneCollection();
			List<String> illegalOrphanMessages = null;
			for (Ligne ligneCollectionOldLigne : ligneCollectionOld) {
				if (!ligneCollectionNew.contains(ligneCollectionOldLigne)) {
					if (illegalOrphanMessages == null) {
						illegalOrphanMessages = new ArrayList<String>();
					}
					illegalOrphanMessages.add("You must retain Ligne " + ligneCollectionOldLigne + " since its commande1 field is not nullable.");
				}
			}
			if (illegalOrphanMessages != null) {
				throw new IllegalOrphanException(illegalOrphanMessages);
			}
			if (clientNew != null) {
				clientNew = em.getReference(clientNew.getClass(), clientNew.getCode());
				commande.setClient(clientNew);
			}
			Collection<Ligne> attachedLigneCollectionNew = new ArrayList<Ligne>();
			for (Ligne ligneCollectionNewLigneToAttach : ligneCollectionNew) {
				ligneCollectionNewLigneToAttach = em.getReference(ligneCollectionNewLigneToAttach.getClass(), ligneCollectionNewLigneToAttach.getLignePK());
				attachedLigneCollectionNew.add(ligneCollectionNewLigneToAttach);
			}
			ligneCollectionNew = attachedLigneCollectionNew;
			commande.setLigneCollection(ligneCollectionNew);
			commande = em.merge(commande);
			if (clientOld != null && !clientOld.equals(clientNew)) {
				clientOld.getCommandeCollection().remove(commande);
				clientOld = em.merge(clientOld);
			}
			if (clientNew != null && !clientNew.equals(clientOld)) {
				clientNew.getCommandeCollection().add(commande);
				clientNew = em.merge(clientNew);
			}
			for (Ligne ligneCollectionNewLigne : ligneCollectionNew) {
				if (!ligneCollectionOld.contains(ligneCollectionNewLigne)) {
					Commande oldCommande1OfLigneCollectionNewLigne = ligneCollectionNewLigne.getCommande1();
					ligneCollectionNewLigne.setCommande1(commande);
					ligneCollectionNewLigne = em.merge(ligneCollectionNewLigne);
					if (oldCommande1OfLigneCollectionNewLigne != null && !oldCommande1OfLigneCollectionNewLigne.equals(commande)) {
						oldCommande1OfLigneCollectionNewLigne.getLigneCollection().remove(ligneCollectionNewLigne);
						oldCommande1OfLigneCollectionNewLigne = em.merge(oldCommande1OfLigneCollectionNewLigne);
					}
				}
			}
			em.getTransaction().commit();
		} catch (Exception ex) {
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				Integer id = commande.getNumero();
				if (findCommande(id) == null) {
					throw new NonexistentEntityException("The commande with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Commande commande;
			try {
				commande = em.getReference(Commande.class, id);
				commande.getNumero();
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The commande with id " + id + " no longer exists.", enfe);
			}
			List<String> illegalOrphanMessages = null;
			Collection<Ligne> ligneCollectionOrphanCheck = commande.getLigneCollection();
			for (Ligne ligneCollectionOrphanCheckLigne : ligneCollectionOrphanCheck) {
				if (illegalOrphanMessages == null) {
					illegalOrphanMessages = new ArrayList<String>();
				}
				illegalOrphanMessages.add("This Commande (" + commande + ") cannot be destroyed since the Ligne " + ligneCollectionOrphanCheckLigne + " in its ligneCollection field has a non-nullable commande1 field.");
			}
			if (illegalOrphanMessages != null) {
				throw new IllegalOrphanException(illegalOrphanMessages);
			}
			Client client = commande.getClient();
			if (client != null) {
				client.getCommandeCollection().remove(commande);
				client = em.merge(client);
			}
			em.remove(commande);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public List<Commande> findCommandeEntities() {
		return findCommandeEntities(true, -1, -1);
	}

	public List<Commande> findCommandeEntities(int maxResults, int firstResult) {
		return findCommandeEntities(false, maxResults, firstResult);
	}

	private List<Commande> findCommandeEntities(boolean all, int maxResults, int firstResult) {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			cq.select(cq.from(Commande.class));
			Query q = em.createQuery(cq);
			if (!all) {
				q.setMaxResults(maxResults);
				q.setFirstResult(firstResult);
			}
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public Commande findCommande(Integer id) {
		EntityManager em = getEntityManager();
		try {
			return em.find(Commande.class, id);
		} finally {
			em.close();
		}
	}

	public int getCommandeCount() {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			Root<Commande> rt = cq.from(Commande.class);
			cq.select(em.getCriteriaBuilder().count(rt));
			Query q = em.createQuery(cq);
			return ((Long) q.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}
	
}
