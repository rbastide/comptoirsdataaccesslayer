/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comptoirs.model.generateddao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import comptoirs.model.entity.Categorie;
import comptoirs.model.entity.Ligne;
import comptoirs.model.entity.Produit;
import comptoirs.model.generateddao.exceptions.IllegalOrphanException;
import comptoirs.model.generateddao.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author RemiB
 */
public class ProduitJpaController implements Serializable {

	public ProduitJpaController(EntityManagerFactory emf) {
		this.emf = emf;
	}
	private EntityManagerFactory emf = null;

	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public void create(Produit produit) {
		if (produit.getLigneCollection() == null) {
			produit.setLigneCollection(new ArrayList<Ligne>());
		}
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Categorie categorie = produit.getCategorie();
			if (categorie != null) {
				categorie = em.getReference(categorie.getClass(), categorie.getCode());
				produit.setCategorie(categorie);
			}
			Collection<Ligne> attachedLigneCollection = new ArrayList<Ligne>();
			for (Ligne ligneCollectionLigneToAttach : produit.getLigneCollection()) {
				ligneCollectionLigneToAttach = em.getReference(ligneCollectionLigneToAttach.getClass(), ligneCollectionLigneToAttach.getLignePK());
				attachedLigneCollection.add(ligneCollectionLigneToAttach);
			}
			produit.setLigneCollection(attachedLigneCollection);
			em.persist(produit);
			if (categorie != null) {
				categorie.getProduitCollection().add(produit);
				categorie = em.merge(categorie);
			}
			for (Ligne ligneCollectionLigne : produit.getLigneCollection()) {
				Produit oldProduit1OfLigneCollectionLigne = ligneCollectionLigne.getProduit1();
				ligneCollectionLigne.setProduit1(produit);
				ligneCollectionLigne = em.merge(ligneCollectionLigne);
				if (oldProduit1OfLigneCollectionLigne != null) {
					oldProduit1OfLigneCollectionLigne.getLigneCollection().remove(ligneCollectionLigne);
					oldProduit1OfLigneCollectionLigne = em.merge(oldProduit1OfLigneCollectionLigne);
				}
			}
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public void edit(Produit produit) throws IllegalOrphanException, NonexistentEntityException, Exception {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Produit persistentProduit = em.find(Produit.class, produit.getReference());
			Categorie categorieOld = persistentProduit.getCategorie();
			Categorie categorieNew = produit.getCategorie();
			Collection<Ligne> ligneCollectionOld = persistentProduit.getLigneCollection();
			Collection<Ligne> ligneCollectionNew = produit.getLigneCollection();
			List<String> illegalOrphanMessages = null;
			for (Ligne ligneCollectionOldLigne : ligneCollectionOld) {
				if (!ligneCollectionNew.contains(ligneCollectionOldLigne)) {
					if (illegalOrphanMessages == null) {
						illegalOrphanMessages = new ArrayList<String>();
					}
					illegalOrphanMessages.add("You must retain Ligne " + ligneCollectionOldLigne + " since its produit1 field is not nullable.");
				}
			}
			if (illegalOrphanMessages != null) {
				throw new IllegalOrphanException(illegalOrphanMessages);
			}
			if (categorieNew != null) {
				categorieNew = em.getReference(categorieNew.getClass(), categorieNew.getCode());
				produit.setCategorie(categorieNew);
			}
			Collection<Ligne> attachedLigneCollectionNew = new ArrayList<Ligne>();
			for (Ligne ligneCollectionNewLigneToAttach : ligneCollectionNew) {
				ligneCollectionNewLigneToAttach = em.getReference(ligneCollectionNewLigneToAttach.getClass(), ligneCollectionNewLigneToAttach.getLignePK());
				attachedLigneCollectionNew.add(ligneCollectionNewLigneToAttach);
			}
			ligneCollectionNew = attachedLigneCollectionNew;
			produit.setLigneCollection(ligneCollectionNew);
			produit = em.merge(produit);
			if (categorieOld != null && !categorieOld.equals(categorieNew)) {
				categorieOld.getProduitCollection().remove(produit);
				categorieOld = em.merge(categorieOld);
			}
			if (categorieNew != null && !categorieNew.equals(categorieOld)) {
				categorieNew.getProduitCollection().add(produit);
				categorieNew = em.merge(categorieNew);
			}
			for (Ligne ligneCollectionNewLigne : ligneCollectionNew) {
				if (!ligneCollectionOld.contains(ligneCollectionNewLigne)) {
					Produit oldProduit1OfLigneCollectionNewLigne = ligneCollectionNewLigne.getProduit1();
					ligneCollectionNewLigne.setProduit1(produit);
					ligneCollectionNewLigne = em.merge(ligneCollectionNewLigne);
					if (oldProduit1OfLigneCollectionNewLigne != null && !oldProduit1OfLigneCollectionNewLigne.equals(produit)) {
						oldProduit1OfLigneCollectionNewLigne.getLigneCollection().remove(ligneCollectionNewLigne);
						oldProduit1OfLigneCollectionNewLigne = em.merge(oldProduit1OfLigneCollectionNewLigne);
					}
				}
			}
			em.getTransaction().commit();
		} catch (Exception ex) {
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				Integer id = produit.getReference();
				if (findProduit(id) == null) {
					throw new NonexistentEntityException("The produit with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Produit produit;
			try {
				produit = em.getReference(Produit.class, id);
				produit.getReference();
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The produit with id " + id + " no longer exists.", enfe);
			}
			List<String> illegalOrphanMessages = null;
			Collection<Ligne> ligneCollectionOrphanCheck = produit.getLigneCollection();
			for (Ligne ligneCollectionOrphanCheckLigne : ligneCollectionOrphanCheck) {
				if (illegalOrphanMessages == null) {
					illegalOrphanMessages = new ArrayList<String>();
				}
				illegalOrphanMessages.add("This Produit (" + produit + ") cannot be destroyed since the Ligne " + ligneCollectionOrphanCheckLigne + " in its ligneCollection field has a non-nullable produit1 field.");
			}
			if (illegalOrphanMessages != null) {
				throw new IllegalOrphanException(illegalOrphanMessages);
			}
			Categorie categorie = produit.getCategorie();
			if (categorie != null) {
				categorie.getProduitCollection().remove(produit);
				categorie = em.merge(categorie);
			}
			em.remove(produit);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public List<Produit> findProduitEntities() {
		return findProduitEntities(true, -1, -1);
	}

	public List<Produit> findProduitEntities(int maxResults, int firstResult) {
		return findProduitEntities(false, maxResults, firstResult);
	}

	private List<Produit> findProduitEntities(boolean all, int maxResults, int firstResult) {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			cq.select(cq.from(Produit.class));
			Query q = em.createQuery(cq);
			if (!all) {
				q.setMaxResults(maxResults);
				q.setFirstResult(firstResult);
			}
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public Produit findProduit(Integer id) {
		EntityManager em = getEntityManager();
		try {
			return em.find(Produit.class, id);
		} finally {
			em.close();
		}
	}

	public int getProduitCount() {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			Root<Produit> rt = cq.from(Produit.class);
			cq.select(em.getCriteriaBuilder().count(rt));
			Query q = em.createQuery(cq);
			return ((Long) q.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}
	
}
