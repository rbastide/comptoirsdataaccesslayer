/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comptoirs.model.generateddao;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import comptoirs.model.entity.Commande;
import comptoirs.model.entity.Ligne;
import comptoirs.model.entity.LignePK;
import comptoirs.model.entity.Produit;
import comptoirs.model.generateddao.exceptions.NonexistentEntityException;
import comptoirs.model.generateddao.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author RemiB
 */
public class LigneJpaController implements Serializable {

	public LigneJpaController(EntityManagerFactory emf) {
		this.emf = emf;
	}
	private EntityManagerFactory emf = null;

	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public void create(Ligne ligne) throws PreexistingEntityException, Exception {
		if (ligne.getLignePK() == null) {
			ligne.setLignePK(new LignePK());
		}
		ligne.getLignePK().setCommande(ligne.getCommande1().getNumero());
		ligne.getLignePK().setProduit(ligne.getProduit1().getReference());
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Commande commande1 = ligne.getCommande1();
			if (commande1 != null) {
				commande1 = em.getReference(commande1.getClass(), commande1.getNumero());
				ligne.setCommande1(commande1);
			}
			Produit produit1 = ligne.getProduit1();
			if (produit1 != null) {
				produit1 = em.getReference(produit1.getClass(), produit1.getReference());
				ligne.setProduit1(produit1);
			}
			em.persist(ligne);
			if (commande1 != null) {
				commande1.getLigneCollection().add(ligne);
				commande1 = em.merge(commande1);
			}
			if (produit1 != null) {
				produit1.getLigneCollection().add(ligne);
				produit1 = em.merge(produit1);
			}
			em.getTransaction().commit();
		} catch (Exception ex) {
			if (findLigne(ligne.getLignePK()) != null) {
				throw new PreexistingEntityException("Ligne " + ligne + " already exists.", ex);
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public void edit(Ligne ligne) throws NonexistentEntityException, Exception {
		ligne.getLignePK().setCommande(ligne.getCommande1().getNumero());
		ligne.getLignePK().setProduit(ligne.getProduit1().getReference());
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Ligne persistentLigne = em.find(Ligne.class, ligne.getLignePK());
			Commande commande1Old = persistentLigne.getCommande1();
			Commande commande1New = ligne.getCommande1();
			Produit produit1Old = persistentLigne.getProduit1();
			Produit produit1New = ligne.getProduit1();
			if (commande1New != null) {
				commande1New = em.getReference(commande1New.getClass(), commande1New.getNumero());
				ligne.setCommande1(commande1New);
			}
			if (produit1New != null) {
				produit1New = em.getReference(produit1New.getClass(), produit1New.getReference());
				ligne.setProduit1(produit1New);
			}
			ligne = em.merge(ligne);
			if (commande1Old != null && !commande1Old.equals(commande1New)) {
				commande1Old.getLigneCollection().remove(ligne);
				commande1Old = em.merge(commande1Old);
			}
			if (commande1New != null && !commande1New.equals(commande1Old)) {
				commande1New.getLigneCollection().add(ligne);
				commande1New = em.merge(commande1New);
			}
			if (produit1Old != null && !produit1Old.equals(produit1New)) {
				produit1Old.getLigneCollection().remove(ligne);
				produit1Old = em.merge(produit1Old);
			}
			if (produit1New != null && !produit1New.equals(produit1Old)) {
				produit1New.getLigneCollection().add(ligne);
				produit1New = em.merge(produit1New);
			}
			em.getTransaction().commit();
		} catch (Exception ex) {
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				LignePK id = ligne.getLignePK();
				if (findLigne(id) == null) {
					throw new NonexistentEntityException("The ligne with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public void destroy(LignePK id) throws NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			Ligne ligne;
			try {
				ligne = em.getReference(Ligne.class, id);
				ligne.getLignePK();
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The ligne with id " + id + " no longer exists.", enfe);
			}
			Commande commande1 = ligne.getCommande1();
			if (commande1 != null) {
				commande1.getLigneCollection().remove(ligne);
				commande1 = em.merge(commande1);
			}
			Produit produit1 = ligne.getProduit1();
			if (produit1 != null) {
				produit1.getLigneCollection().remove(ligne);
				produit1 = em.merge(produit1);
			}
			em.remove(ligne);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public List<Ligne> findLigneEntities() {
		return findLigneEntities(true, -1, -1);
	}

	public List<Ligne> findLigneEntities(int maxResults, int firstResult) {
		return findLigneEntities(false, maxResults, firstResult);
	}

	private List<Ligne> findLigneEntities(boolean all, int maxResults, int firstResult) {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			cq.select(cq.from(Ligne.class));
			Query q = em.createQuery(cq);
			if (!all) {
				q.setMaxResults(maxResults);
				q.setFirstResult(firstResult);
			}
			return q.getResultList();
		} finally {
			em.close();
		}
	}

	public Ligne findLigne(LignePK id) {
		EntityManager em = getEntityManager();
		try {
			return em.find(Ligne.class, id);
		} finally {
			em.close();
		}
	}

	public int getLigneCount() {
		EntityManager em = getEntityManager();
		try {
			CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
			Root<Ligne> rt = cq.from(Ligne.class);
			cq.select(em.getCriteriaBuilder().count(rt));
			Query q = em.createQuery(cq);
			return ((Long) q.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}
	
}
